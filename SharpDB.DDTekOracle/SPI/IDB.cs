﻿using SharpDB.DDTekOracle;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SharpDB.SPI
{
    public interface IDB
    {

        event EventHandler<OnErrorEventArgs> OnError;

        #region Bool查询

        bool TryConnect();

        bool ValidateSql(string strSql, out string msg);


        #endregion

        #region 查询
        TReturn QrySingle<TReturn>(string strSql, params OracleParam[] cmdParms);

        DataRow QryRow(string strSql, params OracleParam[] cmdParms);

        DataTable QryTable(string strSql, params OracleParam[] cmdParms);
        
        List<DataTable> QryDS(string strSql, params OracleParam[] cmdParms);

        DbDataReader ExecReader(string strSql, params OracleParam[] cmdParms);

        DataSet QryDSByProc(string storedProcName, params OracleParam[] cmdParms);

        DataTable QryTableByProc(string storedProcName, params OracleParam[] cmdParms);

        #endregion

        #region 执行

        int ExecSql(string strSql, params OracleParam[] cmdParms);

        int ExecSqlTran(params string[] sqlCmds);

        int ExecSqlTran(Hashtable strSqlList);

        int ExecProc(string procName, params OracleParam[] cmdParms);

        bool BulkCopy(DataTable data, string tableName, Dictionary<string, string> columnMappings = null,
            int batchSize = 200000, int bulkCopyTimeout = 60);

        bool BulkCopy(DbDataReader reader, string tableName, Dictionary<string, string> columnMappings = null,
            int batchSize = 200000, int bulkCopyTimeout = 60);

        #endregion
    }
}
