﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data
{
    public enum DBType
    {
        SqlServer,
        MySql,
        OracleManaged,
        OracleDDTek,
        PostgreSql,
        SQLite
    }
}
