﻿using MSharp.Data.DatabaseInfo;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data.Database
{
    public   class SQLiteDB : DB
    {
        public SQLiteDB(DBType dbType, DbProviderFactory dbFactory, string ConnectionString) 
            : base(dbType, dbFactory, ConnectionString)
        {
            this.Info = new SQLiteDBInfo(this);
        }
    }
}
