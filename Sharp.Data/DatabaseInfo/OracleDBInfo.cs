﻿using MSharp.Data.SPI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MSharp.Data.DatabaseInfo
{
    public class OracleDBInfo : IDBInfo
    {
        private DB Db { get; set; }
        
        public OracleDBInfo(DB db)
        {
            this.Db = db;
            Refresh();
        }
        
        public string DBName
        {
            get
            {
                if (Db.ConnectionStringBuilder is Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder)
                {
                    //127.0.0.1:1521/CTMS
                    string source = (Db.ConnectionStringBuilder as Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder).DataSource;
                    return Regex.Replace(source, @"(.+/)(.+)", "$2");
                }
                else
                {
                    return (Db.ConnectionStringBuilder as DDTek.Oracle.OracleConnectionStringBuilder).ServiceName;
                }
            }
        }

        public NameValueCollection TableComments { get; private set; }
        public List<string> TableNames { get; private set; }
        public Dictionary<string, TableInfo> TableInfoDict { get; private set; }
        public Dictionary<string, List<string>> TableColumnNameDict { get; private set; }
        public Dictionary<string, List<ColumnInfo>> TableColumnInfoDict { get; private set; }

        public Dictionary<string, NameValueCollection> TableColumnComments { get; private set; }

        private Dictionary<string, ColumnInfo> DictColumnInfo { get; set; }

        /// <summary>
        /// 所有表名 对应的自增列的 序列名
        /// </summary>
        private Dictionary<string, string> Dict_Table_Sequence { get; set; }


        public ColumnInfo this[string tableName, string columnName]
        {
            get
            {
                ColumnInfo colInfo;
                var strKey = (tableName + "@" + columnName).ToUpper();
                DictColumnInfo.TryGetValue(strKey, out colInfo);
                return colInfo;
            }
        }

        public List<string> this[string tableName]
        {
            get
            {
                tableName = (tableName ?? string.Empty).ToUpper();
                List<string> colNames;
                TableColumnNameDict.TryGetValue(tableName, out colNames);
                return colNames;
            }
        }

        /// <summary>
        /// 所有序列名
        /// </summary>
        public List<string> Sequences { get; set; }

        /// <summary>
        /// 由于Oralce没有库名,只有表空间,所以Oracle没有提供数据库名称查询支持。
        /// </summary>
        public List<string> DBNames { get { return DBName.TransList(); } }


        /// <summary>
        /// 查询当前表的自增列的序列
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns>序列名称</returns>
        public string IdentitySeqName(string tableName)
        {
            string seqName;
            tableName = (tableName ?? string.Empty).ToUpper();
            if (Dict_Table_Sequence.TryGetValue(tableName, out seqName))
            {
                return seqName;
            }
            return string.Empty;
        }
        
        public bool Refresh()
        {
            try
            {

                #region 查询Oracle的所有序列
                this.Dict_Table_Sequence = new Dictionary<string, string>();

                string sequence_Sql = "select sequence_name from user_sequences";
                this.Sequences = Db.ReadList<string>(sequence_Sql);
                
                #endregion
                
                string strSql = "Select table_Name As Name,Comments As Value From User_Tab_Comments Where table_Type='TABLE' Order By table_Name Asc";
                this.TableComments = Db.ReadNameValues(strSql);

                if (TableComments != null && TableComments.Count > 0)
                {
                    this.TableNames = TableComments.AllKeys.ToList();

                    this.TableInfoDict = new Dictionary<string, TableInfo>();
                    this.TableColumnNameDict = new Dictionary<string, List<string>>();
                    this.TableColumnInfoDict = new Dictionary<string, List<ColumnInfo>>();
                    this.TableColumnComments = new Dictionary<string, NameValueCollection>();

                    this.DictColumnInfo = new Dictionary<string, ColumnInfo>();

                    foreach (string tableName in TableNames)
                    {
                        TableInfo tabInfo = new TableInfo();
                        tabInfo.TableName = tableName;
                        tabInfo.TabComment = TableComments[tableName];

                        /** 该语句，包含某列是否自增列，查询慢 **/
     
                        strSql = @"select a.COLUMN_ID As Colorder,a.COLUMN_NAME As ColumnName,a.DATA_TYPE As TypeName,b.comments As DeText,(Case When a.DATA_TYPE='NUMBER' Then a.DATA_PRECISION Else a.DATA_LENGTH End )As Length,a.DATA_SCALE As Scale,
	(Case When (select Count(1)  from user_cons_columns aa, user_constraints bb where aa.constraint_name = bb.constraint_name 
	 and bb.constraint_type = 'P' and aa.table_name = '{0}' And aa.column_name=a.COLUMN_NAME)>0 Then 1 Else 0 End
	 ) As IsPK,(
			 case when (select count(1) from user_triggers tri INNER JOIN user_source src on tri.trigger_Name=src.Name 
				where (triggering_Event='INSERT' and table_name='{0}')
			and regexp_like(text,	concat(concat('nextval\s+into\s*?:\s*?new\s*?\.\s*?',a.COLUMN_NAME),'\s+?'),'i'))>0 
			then 1 else 0 end 
	) As IsIdentity, 
		Case a.NULLABLE  When 'Y' Then 1 Else 0 End As CanNull,
		a.data_default As DefaultVal from user_tab_columns a Inner Join user_col_comments b On a.TABLE_NAME=b.table_name 
	Where b.COLUMN_NAME= a.COLUMN_NAME   and a.Table_Name='{0}'  order by a.column_ID Asc";

                        if (Db.DBType== DBType.OracleDDTek)
                        {
                            strSql = strSql.Replace("'{0}'", "?");
                            tabInfo.Colnumns = Db.QueryTable(strSql, new { t1 = tableName.ToUpper(), t2 = tableName.ToUpper(), t3 = tableName.ToUpper() }).ConvertToListObject<ColumnInfo>();

                        }
                        else
                        {
                            strSql = strSql.Replace("'{0}'", ":" + tableName);
                            tabInfo.Colnumns = Db.QueryTable(strSql, new { tableName = tableName.ToUpper() }).ConvertToListObject<ColumnInfo>();
                        }
                        
                        List<string> lstColName = new List<string>();
                        NameValueCollection nvcColDeText = new NameValueCollection();
                        foreach (ColumnInfo colInfo in tabInfo.Colnumns)
                        {
                            lstColName.Add(colInfo.ColumnName);
                            nvcColDeText.Add(colInfo.ColumnName, colInfo.DeText);

                            var strKey = (tableName + "@" + colInfo.ColumnName).ToUpper();
                            DictColumnInfo.Add(strKey, colInfo);

                            //自增的列，需要查询序列名称
                            if (colInfo.IsIdentity)
                            {
                                AddColSeq(tableName, colInfo.ColumnName);
                            }

                            if (colInfo.IsPK)
                            {
                                tabInfo.PriKeyColName = colInfo.ColumnName;
                                if (colInfo.IsIdentity)
                                {
                                    tabInfo.PriKeyType = PrimaryKeyType.AUTO;
                                }
                                else
                                {
                                    tabInfo.PriKeyType = PrimaryKeyType.SET;
                                }
                            }
                        }

                        TableInfoDict.Add(tableName, tabInfo);
                        TableColumnNameDict.Add(tableName, lstColName);
                        TableColumnInfoDict.Add(tableName, tabInfo.Colnumns);
                        TableColumnComments.Add(tableName, nvcColDeText);

                    }

                }

                    
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 查询自增序列的 序列名称
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="colName">列名</param>
        private void AddColSeq(string tableName,string colName)
        {
            string strSql = string.Empty;
            if (Sequences != null && Sequences.Count > 0)
            {
                foreach (string  seqName in Sequences)
                {
                    strSql = @"select count(1) from user_triggers tri INNER JOIN user_source src on tri.trigger_Name=src.Name where (triggering_Event='INSERT' and table_name='" + tableName + "') and regexp_like(text,concat(concat('" + seqName + @"\s*?\.\s*?nextval\s+into\s*?:\s*?new\s*?\.\s*?','" + colName + @"'),'\s+?'),'i')";
                    int res = Db.Single<int>(strSql,0);
                    if (res > 0)
                    {
                        Dict_Table_Sequence[tableName] = seqName;
                        break;
                    }
                }
            }
        }

        public bool IsExistTable(string tableName)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            return TableNames.Contains(tableName);
        }

        public bool IsExistColumn(string tableName, string columnName)
        {
            var strKey = (tableName + "@" + columnName).ToUpper();
            return DictColumnInfo.ContainsKey(strKey);
        }

        public Dictionary<string, DateTime> GetTableStruct_Modify()
        {
            string strSql = "select object_name as name ,last_ddl_time as modify_date from user_objects Where object_Type='TABLE' Order By last_ddl_time Desc";
            return Db.ReadDictionary<string, DateTime>(strSql);
        }

        public string GetColumnComment(string tableName, string columnName)
        {
            ColumnInfo colInfo = null;
            var strKey = (tableName + "@" + columnName).ToUpper();
            DictColumnInfo.TryGetValue(strKey, out colInfo);
            return colInfo?.DeText;
        }


        public string GetTableComment(string tableName)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            return TableComments[tableName];
        }

        public List<ColumnInfo> GetColumns(string tableName)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            List<ColumnInfo> colInfos = null;
            TableColumnInfoDict.TryGetValue(tableName, out colInfos);
            return colInfos;
        }


        public bool SetTableComment(string tableName, string comment)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            if (!TableNames.Contains(tableName))
            {
                //throw new ArgumentException("该表不存在！" + tableName, "tableName");
                return false;
            }

            string upsert_sql = string.Empty;
            comment = (comment ?? string.Empty).Replace("'", "");
            try
            {
                upsert_sql = @"comment on table " + tableName + " is '" + comment + "'";
                Db.ExecSql(upsert_sql);

                TableComments[tableName] = comment;

                var tabInfo = TableInfoDict[tableName];
                tabInfo.TabComment = comment;
                TableInfoDict[tableName] = tabInfo;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        public bool SetColumnComment(string tableName, string columnName, string comment)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            columnName = (columnName ?? string.Empty).ToUpper();

            if (!TableNames.Contains(tableName))
            {
                //throw new ArgumentException("该表不存在！" + tableName, "tableName");
                return false;
            }

            if (!DictColumnInfo.ContainsKey(tableName + "@" + columnName))
            {
                //throw new ArgumentException(tableName + "表不存在" + columnName + "列！", "columnName");
                return false;
            }

            string upsert_sql = string.Empty;
            comment = (comment ?? string.Empty).Replace("'", "");
            try
            {
                upsert_sql = @"comment on column " + tableName + "." + columnName + " is '" + comment + "'";
                Db.ExecSql(upsert_sql);

                List<ColumnInfo> lstColInfo = TableColumnInfoDict[tableName];

                NameValueCollection nvcColDesc = new NameValueCollection();
                lstColInfo.ForEach(t =>
                {
                    if (t.ColumnName.Equals(columnName))
                    {
                        t.DeText = comment;
                    }
                    nvcColDesc.Add(t.ColumnName, t.DeText);
                });

                TableColumnInfoDict.Remove(tableName);
                TableColumnInfoDict.Add(tableName, lstColInfo);

                TableColumnComments.Remove(tableName);
                TableColumnComments.Add(tableName, nvcColDesc);

                var strKey = (tableName + "@" + columnName).ToUpper();
                ColumnInfo colInfo = DictColumnInfo[strKey];
                colInfo.DeText = comment;
                DictColumnInfo[strKey] = colInfo;

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool DropTable(string tableName)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            if (!TableNames.Contains(tableName))
            {
                throw new ArgumentException("该表不存在！" + tableName, "tableName");
            }

            string drop_sql = string.Empty;
            try
            {

                drop_sql = "drop table " + tableName;
                Db.ExecSql(drop_sql);

                this.TableComments.Remove(tableName);

                this.TableNames = TableComments.AllKeys.ToList();

                this.TableInfoDict.Remove(tableName);
                this.TableColumnInfoDict.Remove(tableName);
                this.TableColumnComments.Remove(tableName);

                var lstColName = TableColumnNameDict[tableName];

                foreach (var colName in lstColName)
                {
                    var strKey = (tableName + "@" + colName).ToUpper();
                    this.DictColumnInfo.Remove(strKey);
                }

                this.TableColumnNameDict.Remove(tableName);

            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }


        public bool DropColumn(string tableName, string columnName)
        {
            tableName = (tableName ?? string.Empty).ToUpper();
            columnName = (columnName ?? string.Empty).ToUpper();
            if (!TableNames.Contains(tableName))
            {
                throw new ArgumentException("该表不存在！" + tableName, "tableName");
            }

            var strKey = (tableName + "@" + columnName).ToUpper();

            if (!DictColumnInfo.ContainsKey(strKey))
            {
                throw new ArgumentException(tableName + "表不存在" + columnName + "列！", "columnName");
            }

            try
            {
                string drop_sql = "alter table {0} drop column {1}";
                drop_sql = string.Format(drop_sql, tableName, columnName);
                Db.ExecSql(drop_sql);

                this.DictColumnInfo.Remove(strKey);

                var nvc = TableColumnComments[tableName];
                nvc.Remove(columnName);
                TableColumnNameDict[tableName] = nvc.AllKeys.ToList();

                var lstColInfo = TableColumnInfoDict[tableName];
                ColumnInfo curColInfo = null;
                lstColInfo.ForEach(t =>
                {
                    if (t.ColumnName.Equals(columnName))
                    {
                        curColInfo = t;

                        //tabInfo 对应的 主键类型和主键列 也需要 跟着修改。
                        if (curColInfo.IsPK)
                        {
                            var tabInfo = TableInfoDict[tableName];
                            tabInfo.PriKeyType = PrimaryKeyType.UNKNOWN;
                            tabInfo.PriKeyColName = null;
                            TableInfoDict[tableName] = tabInfo;
                        }
                        return;
                    }
                });
                lstColInfo.Remove(curColInfo);
                TableColumnInfoDict[tableName] = lstColInfo;

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
