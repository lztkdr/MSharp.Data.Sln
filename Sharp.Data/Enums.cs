﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data
{
    public enum DBType
    {
        SqlServer,
        MySql,
        Oracle,
        OracleDDTek,
        PostgreSql,
        SQLite
    }

    /// <summary>
    /// 主键类型
    /// </summary>
    public enum PrimaryKeyType
    {
        /// <summary>
        /// 没有主键或未知
        /// </summary>
        UNKNOWN,

        /// <summary>
        /// 主键的值是自增方式
        /// </summary>
        AUTO,

        /// <summary>
        /// 主键的值是插入前设置的方式
        /// </summary>
        SET
    }
}
