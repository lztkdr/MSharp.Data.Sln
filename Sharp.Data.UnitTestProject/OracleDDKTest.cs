﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSharp.Data.SPI;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data.UnitTestProject
{
    [TestClass]
    public class OracleDDKTest
    {
        public static DB db = DBMgr.Connect(DBType.OracleDDTek, "127.0.0.1", 1521, "CTMS", "GIS_DMZ_DB", "123456");

        static OracleDDKTest()
        {
            db.OnExecuting = (sql, pars) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnError = (sql, pars, exp) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnExecutingChange = (sql, pars) =>
            {
                return new KeyValuePair<string, DbParameter[]>(sql, pars.ToArray());
            };

            db.OnExecuted = (sql, pars, res) =>
            {
                var arrPar = pars.ToArray();
            };

        }

        /// <summary>
        /// 拼接in sql语句
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="columnName">列名</param>
        /// <param name="values">元素值</param>
        /// <param name="isNotIn">not in 或 in </param>
        /// <returns></returns>
        public static string SqlIn<T>(string columnName, T[] values, bool isNotIn = false)
        {
            string result = string.Empty;
            if (values == null || values.Length <= 0)
            {
                return string.Empty;
            }
            List<string> lst = new List<string>();
            foreach (T obj in values)
            {
                if (obj != null)
                {
                    string val = obj.ToString();
                    if (val.StartsWith("'") && val.EndsWith("'"))
                    {
                        val = val.Replace("'", "'''");
                        lst.Add(val);
                        continue;
                    }
                    lst.Add("'" + val + "'");
                }
            }
            if (lst.Count > 0)
            {
                result = " and " + columnName + " " + (isNotIn ? "not" : "") + " in (" + string.Join(",", lst) + ") ";
            }
            return result;
        }

        [TestMethod]
        public void Test()
        {
            DataTable data = db.QueryTable("select * from GPSINFORMATION where BMAPADDRESS  is not null and gpstime > to_date('2017-09-01','yyyy-mm-dd hh24:mi:ss')");
            
            var lst = DataToList.ToList<GPSINFORMATION>(data);

            var groups = lst.GroupBy(t => t.BMAPADDRESS);

            List<string> lstUseGPS = new List<string>();
            foreach (var gp in groups)
            {
                var lstGPS = gp.ToList();
                int len = lstGPS.Count;
                if (len > 2)
                {
                    lstUseGPS.Add(lstGPS[len / 2].GPSID);
                }
                else
                {
                    lstUseGPS.Add(lstGPS.FirstOrDefault().GPSID);
                }
            }
            
            string strSql = "select b.ENGMODEL,'1' as OPERATETYPEID,a.BMAPLONGITUDE,a.BMAPLATITUDE,a.GPSTIME,a.GPSID from GPSINFORMATION a right join ENGINFORMATION b on a.engid = b.engid where a.gpsId is not null ";

            strSql += (" " + SqlIn("gpsId", lstUseGPS.ToArray()));

            strSql += "order by a.gpsTime asc";

            DataTable dataTmp = db.QueryTable(strSql);


            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTmp);


            File.WriteAllText("e:\\json.txt", json, Encoding.UTF8);
            
        }


        

        [TestMethod]
        public void Test2()
        {
            string strSql = "select * from SYSTEMMENU where INTLEVEL=?";
            DataTable data = db.QueryTable(strSql, new { ID = 1 });

            var info = db.Info;

            var lst = info.GetColumns("SYSTEMMENU");
            
        }

        [TestMethod]
        public void Test3()
        {
            db = DBMgr.Connect(DBType.OracleDDTek, "127.0.0.1", 1521, "CTMS", "SCOTT", "123456");

            db.OnExecuting = (sql, pars) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnError = (sql, pars, exp) =>
            {
                var arrPar = pars.ToArray();
            };

            db.OnExecutingChange = (sql, pars) =>
            {
                return new KeyValuePair<string, DbParameter[]>(sql, pars.ToArray());
            };

            db.OnExecuted = (sql, pars, res) =>
            {
                var arrPar = pars.ToArray();
            };

            bool bl1 = db.Insert(new { Name = "xiaoqiang", Age = 20 }, "Person");

            int in1 = db.InsertGetInt(new { Name = "xiaoqiang1", Age = 20 }, "Person");

            long lg1 = db.InsertGetLong(new { Name = "xiaoqiang2", Age = 20 }, "Person");

        }


        [TestMethod]
        public void TestMethod3()
        {
            db = DBMgr.Connect(DBType.OracleDDTek, "127.0.0.1", 1521, "CTMS", "SCOTT", "123456");

            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("name", "xiaomeng" + new Random().Next());
            nvc.Add("Age", "30");
            nvc.Add("id", "21");

            bool bl1 = db.Update(nvc, "Person");

            bool bl2 = db.Update(new { name = "xiaoxiao", Id = "22" }, "Person");

            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("name", "ABCDEF");
            dict.Add("Id", "23");

            bool bl3 = db.Update(dict, "Person");
        }
    }
}
