﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSharp.Data.UnitTestProject
{
    public class TypeInfo<T>
    {
        static TypeInfo()
        {
            Name = typeof(T).Name;
        }
        public static string Name { get; private set; }

    }
}
