﻿using MSharp.Data;
using SharpDB;
//using SqlSugar;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Person
    {
        public static string str
        {
            get;set;
        }

        public Person()
        {
            str = new Random().NextDouble().ToString();
        }
        public string Name { get; set; }

        public int Age { get; set; }
    }

    public class ChinaPerson 
    {
        public ChinaPerson()
        {
            
        }

        public int Id { get; set; }

        public Person Per { get; set; }
    }



    class Program
    {
         
        static Program()
        {
           
        }

        public static void Exec<T, X>()
        {
             
        }

        public static DB db = DBMgr.Connect(DBType.SqlServer, "TAKETRY", 1433, "QTP2008", "sa", "123456");

        static void Main(string[] args)
        {
            /***
             *  todo:
             *      1. 所有执行Sql的 Param 参数，如果有 输出参数，该怎么指定？ dapper 怎么处理，暂时未知。
             *      2. Sql命令执行 耗时参数 未定义设定的地方。
             *      3. 使用dataReader 替换 DataAdapter 
             *              ado.net dbreader对象由于不需要fill的过程，读取速度也较快（虽然赶不上COPY)，也可优先考虑。
             *              假如我们单独使用DataReader也可以把数据写入到业务类或者dataset里。那只是根据业务需要而选择不同的数据载体而已。
             *              实际上我们从数据库获得数据都会通过DataReader，只不过DataAdapter把这一切都封装起来了
             *              与DataAdapter相比，DataReader被认为在内存使用上效率更高。 当你不需要在内存中保存数据的副本时，这很有用。
                    4.
             */


            //var info = db.Info;

            //var lstName = info.TableNames;

            //foreach (var name in lstName)
            //{
            //    if (name.Equals("Person"))
            //    {
            //        var lstColInfo = info.TableColumnComments[name];
            //        var lstColName = info.TableColumnNameDict[name];

            //        info.DropColumn(name, "Age");

            //        var lstColInfo2 = info.TableColumnComments[name];
            //        var lstColName3 = info.TableColumnNameDict[name];

            //        Console.WriteLine(lstColInfo.Count == lstColInfo2.Count);
            //        Console.WriteLine(lstColName.Count == lstColName.Count);

            //        var tabInfo = info.TableInfoDict[name];
            //        var lstColInfo3 = tabInfo.Colnumns;
            //        Console.WriteLine(lstColInfo.Count == lstColInfo3.Count);
            //        var nvc = info.TableColumnComments[name];
            //        Console.WriteLine(lstColInfo.Count == nvc.Count);
            //    }
            //}

            int forInt = 100000;
            int forMs = 100;

            //Console.WriteLine("Dictionary：" + new Action(() =>
            //  {
            //      Dictionary<string, string> dict = new Dictionary<string, string>();
            //      for (int j = 0; j < forInt; j++)
            //      {
            //          dict.Add(j.ToString(), j.ToString());
            //      }

            //      foreach (var item in dict)
            //      {
            //          var key = item.Key;
            //          var val = item.Value;
            //      }


            //  }).ForWatchMs(forMs));

            //SqlSugar.SqlSugarClient db = new SqlSugar.SqlSugarClient(null);
            //db.IgnoreInsertColumns.Add(new SqlSugar.IgnoreColumn() { });
            //db.MappingColumns.Remove(new SqlSugar.MappingColumn());

            //Dapper.SqlMapper.QueryMultiple(null, new Dapper.CommandDefinition());


            //http://www.codeisbug.com/Doc/8/1144

            //http://www.cnblogs.com/lzrabbit/archive/2012/04/22/2465313.html

            //http://www.cnblogs.com/lzrabbit/archive/2012/04/22/2465313.html


            //var info = db.Info;

            //var lstName = info["jc_User"];

            //foreach (var colName in lstName)
            //{
            //    var colInfo = info["jc_User", colName];

            //    var dp = db.CreateParameter("YongHM", "xiaoqiang", colInfo);

            //    var dp2 = dp.Clone();
            //}


           


            DB db = DBMgr.Connect(DBType.SqlServer, "TAKETRY", 1433, "QTP2008", "sa", "123456");

            var lstName = db.Info.TableNames;

            int cnt = 1000;

            Console.WriteLine("QueryTable:\t" + StopwatchExtension.ForWatchSec(() =>
            {
                TaskFactory tf1 = Task.Factory;
                List<Task> lstTsk1 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk1.Add(tf1.StartNew(() =>
                    {
                        var data = db.QueryTable("Select top 10 * From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk1.ToArray());
                

            }, cnt));

            Console.WriteLine("ReaderTable:\t" + StopwatchExtension.ForWatchSec(() =>
            {

                TaskFactory tf2 = Task.Factory;
                List<Task> lstTsk2 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk2.Add(tf2.StartNew(() =>
                    {
                        var data = db.ReadTable("Select  top 10 * From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk2.ToArray());
            }, cnt));


            Console.WriteLine("ReaderRow:\t" + StopwatchExtension.ForWatchSec(() =>
            {

                TaskFactory tf2 = Task.Factory;
                List<Task> lstTsk2 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk2.Add(tf2.StartNew(() =>
                    {
                        var data = db.ReadFirstRow("Select  top 1 * From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk2.ToArray());
            }, cnt));


            Console.WriteLine("QueryRow:\t" + StopwatchExtension.ForWatchSec(() =>
            {

                TaskFactory tf2 = Task.Factory;
                List<Task> lstTsk2 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk2.Add(tf2.StartNew(() =>
                    {
                        var data = db.FirstRow("Select  top 1 * From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk2.ToArray());
            }, cnt));


            Console.WriteLine("ReaderSingle:\t" + StopwatchExtension.ForWatchSec(() =>
            {

                TaskFactory tf2 = Task.Factory;
                List<Task> lstTsk2 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk2.Add(tf2.StartNew(() =>
                    {
                        var data = db.ReadSingle<object>("Select  count(1) From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk2.ToArray());
            }, cnt));


            Console.WriteLine("QuerySingle:\t" + StopwatchExtension.ForWatchSec(() =>
            {

                TaskFactory tf2 = Task.Factory;
                List<Task> lstTsk2 = new List<Task>();
                foreach (var tableName in lstName)
                {
                    lstTsk2.Add(tf2.StartNew(() =>
                    {
                        var data = db.Single<object>("Select  count(1) From [" + tableName + "]", 30);
                    }));
                }
                Task.WaitAll(lstTsk2.ToArray());
            }, cnt));

          

            Console.ReadKey();
        }


        public static void Test()
        {

           
        }


     
    }
}
