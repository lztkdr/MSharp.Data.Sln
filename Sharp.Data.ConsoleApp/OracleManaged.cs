﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace ConsoleApp
{
    public class OracleManaged
    {
        public static void Insert()
        {
            string strConn = "Data Source=127.0.0.1:1521/CTMS;User Id=GIS_DMZ_DB;password=123456;Pooling=true;";
            var conn = new OracleConnection(strConn);


            var cmd = conn.CreateCommand();
            var para = cmd.CreateParameter();


            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("id", 120);
                ht.Add("name", "admin");
                ht.Add("age", 120);


                dynamic dyn = new ExpandoObject();
                dyn.id = 111;
                dyn.name = "aaa";
                dyn.age = 111;

                int res = Dapper.SqlMapper.Execute(conn, "insert into Person(id,name,age)values(:id,:name,:age)", new { id = 200, name = "xiaoqiang", age = 20 });
                int res2 = Dapper.SqlMapper.Execute(conn, "insert into Person(id,name,age)values(:id,:name,:age)", dyn);

              
                

            }
            catch (Exception ex)
            {
               

            }
        }
    }
}
